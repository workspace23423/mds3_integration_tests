const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../index')
const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)

// ---> DEBUT
/**
  * Génération des nouvelles couleurs et enregistrement de ces
  * couleurs dans un tableau.
  */
 const newValues = []
 const colorKey = 'NEW_COLOR_'
 let nextCursor = 0;
 const payloadColor = () => {
   const nextColor = `${colorKey}${nextCursor}`
   newValues.push(nextColor)
   nextCursor++;
   return { 'color': nextColor }
 }
 const getCurrentColor = () => {
   return nextCursor > 0 ? `${colorKey}${nextCursor - 1}` : `${colorKey}O`
 }
 // <-- FIN

describe('App', () => {
  // Récupére la liste des couleurs
  it('it should return all colors', () => {
    return chai.request(app)
      .get('/colors')
      .then((res) => {
        expect(res).to.have.status(200); // je vérifie le status 200
        expect(res).to.be.json; // je vérifie qu'il s'agit de JSON
        expect(res.body).to.be.an('object'); // je vérifie que le body est bien un objet
        expect(res.body.results).to.be.an('array'); // je vérifie que le résulat du body est bien un array
      })
  })

    // Récupère un path invalid
   it('should return Not Found', () => {
     return chai.request(app)
       .get('/notfound')
       .then((res) => {
         throw new Error('Path exists!');
       })
       .catch((err) => {
         expect(err).to.have.status(404); // je vérifie le status 400
       });
   });

   //Post une nouvelle couleur
   it('should add new color', () => {
     return chai.request(app)
      .post('/colors')
      .send(payloadColor())
      .then(function(res) {
        expect(res).to.have.status(201); // je vérifie le status 201
        expect(res).to.be.json; // je vérifie qu'il s'agit de json
        expect(res.body).to.be.an('object'); // je vérifie que le body est bien un objet
        expect(res.body.results).to.be.an('array').to.includes(getCurrentColor()) // vérifie que le contenu inclut bien la nouvelle valeur ajoutée
    });
  });

  // Récupére la nouvelle liste de couleur
  it('it should return new color list Request', () => {
    return chai.request(app)
      .get('/colors')
      .then((res) => {
        expect(res).to.have.status(200); // je vérifie le status 200
        expect(res).to.be.json; // je vérifie qu'il s'agit de JSON
        expect(res.body).to.be.an('object'); // je vérifie que le body est bien un objet
        expect(res.body.results).to.be.an('array'); // je vérifie que le résulat du body est bien un array
      })
  })
})
